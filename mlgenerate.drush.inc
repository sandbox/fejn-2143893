/**
 * Implementation of hook_drush_help(). Basic drush help
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'. This hook is optional. If a command
 * does not implement this hook, the command's description is used instead.
 *
 * This hook is also used to look up help metadata, such as help
 * category title and summary.  See the comments below for a description.
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function mlgenerate_drush_help($section) {
  dpm($section, "DRUSH_HELP");
  switch ($section) {
    case 'drush:mlgenerate':
      $help = dt('Automates generating bs for your multinational sites. ');
      $help .= dt('If no language is specified, defaults to none => 'Lorem ipsum'. ');
      $help .= dt('If no character set is specified, defaults to ASCII -> ISO-8859-1.');
			$help .= dt('If no format specified, generates paragraph formatted content.').
      return $help;
    case 'drush:mlgenerate-language':
      return dt('List of languages that content can be generated for.');
    case 'drush:mlgenerate-encoding':
      return dt('List of encodings (character sets) available');
    case 'drush:mlgenerate-items':
      return dt('List items(nodes, articles, etc.) content can be generated for.');
    case 'drush:mlgenerate-formats':
      return dt('List available content formats(paragraph, etc.) that can be generated.');
    // The 'title' meta item is used to name a group of
    // commands in `drush help`.  If a title is not defined,
    // the default is "All commands in ___", with the
    // specific name of the commandfile (e.g. sandwich).
    // Command files with less than four commands will
    // be placed in the "Other commands" section, _unless_
    // they define a title.  It is therefore preferable
    // to not define a title unless the file defines a lot
    // of commands.
    case 'meta:mlgenerate:title':
      return dt("ML commands");
    // The 'summary' meta item is displayed in `drush help --filter`,
    // and is used to give a general idea what the commands in this
    // command file do, and what they have in common.
    case 'meta:mlgenerate:summary':
      return dt("Automates generating bs for your multinational sites.");						
  }
}


/**
 * Implementation of hook_drush_command().
 */
function mlgenerate_drush_command() {
  $items = array();
  // Name of the drush command.
  $items['ml-generate'] = array(
    'description' => 'generate dummy m/l content for a site',
    'callback' => 'drush_mlgenerate_ml_generate',
  );
  return $items;
}
 
/**
 * Callback function for drush ml-generate. 
 * Callback is called by using drush_hook_command() where
 * hook is the name of the module (MYMODULE) and command is the name of
 * the Drush command with all "-" characters converted to "_" characters (my_command)
 *
 * @param $arg1
 *   An optional argument
 */
function drush_mlgenerate_ml_generate($arg1 = NULL) {
  //check if the argument was passed in and just print it out
  if (isset($arg1)) {
   drush_print($arg1);
  }
 
  //log to the command line with an OK status
  drush_log('Running ml-generate', 'ok');
}

/**
 * Implements hook_drush_command().
function mlgenerate_drush_command() {
  $items = array();
  $items['ml-generate'] = array(
    'description' => dt('generate dummy m/l content.'),
    'arguments'   => array(
      'arg1'    => dt('An optional example argument'),
    ),
    'examples' => array(
      'Standard example' => 'drush mlgen',
      'Argument example' => 'drush mlgen -l "Chinese" -t "paragraph" -n 5',
    ),
    'aliases' => array('mlgen'),
  );
  return $items;
}
 */
